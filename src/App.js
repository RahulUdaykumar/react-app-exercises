import React, { Component } from 'react';
import logo from './logo.svg';
import { Navbar, NavbarBrand} from 'reactstrap';
import './App.css';
import Menu from './components/MenuComponent';
import { DISHES } from './shared/dishes';
import DishDetails from './components/DishdetailComponent';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dishes: DISHES
    };
  }
  render() {
    return (
      <div className="App">
        <Navbar className="navbar navbar-dark bg-dark">
          <div className="container">
            <NavbarBrand href="/">Ristrorante Con Fusion</NavbarBrand>
          </div>
        </Navbar>
        <Menu dishes={this.state.dishes} />
        {/* <DishDetails dishes={this.state.dishes} /> */}
      </div>
    );
  }
}

export default App;
